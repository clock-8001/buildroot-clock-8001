# Configuration files for hyperpixel 4 square display from pimoroni

These files can be used to get the clock running with a square 720x720 display from pimoroni: https://shop.pimoroni.com/products/hyperpixel-4-square

Copy the config.txt file to the root of the sd-card and the hyperpixel4.dtbo to overlays/ directory on the sd-card