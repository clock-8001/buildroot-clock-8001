# Configuration files for pimoroni hyperpixel 4 rectangular

The configuration files can be used to get the clock working with a hyperpixel 4 rectangular 800x480 display: https://shop.pimoroni.com/products/hyperpixel-4

Copy the config.txt to the root of the sd card and the hyperpixel4.dtbo to the overlays/ directory.